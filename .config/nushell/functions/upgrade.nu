def upgrade [
    --assumeyes (-y) # Answer yes for all questions
] {
    echo $"(tput bold)Upgrading System...(tput sgr0)"
    if $assumeyes {
        sudo dnf upgrade -y
    } else {
        sudo dnf upgrade
    }

    echo $"\n(tput bold)Updating Flatpaks...(tput sgr0)"
    if $assumeyes {
        flatpak update -y
    } else {
        flatpak update
    }
    
    echo $"\n(tput bold)Updating packages from Cargo...(tput sgr0)"
    if $assumeyes or (
        input 'Check for Updates? [y/N]:'
        | str starts-with -i 'y'
    ) {
        if not (
            cargo install --list
            | str contains "cargo-update"
        ) {
            cargo install cargo-update
        }

        cargo install-update --all
    }
    
    echo $"\n(tput bold)Updating Rust Compiler...(tput sgr0)"
    if $assumeyes or (
        input 'Update Rust Compiler? [y/N]:'
        | str starts-with -i 'y'
    ) {
        rustup update
    }

    echo $"\n(tput bold)Updating TLDR Cache...(tput sgr0)"
    tldr --update
    typst update
}
