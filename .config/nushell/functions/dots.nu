# Set default location
$env.DOTFILES = if "DOTFILES" in $env { $env.DOTFILES
    } else {
        $env.HOME | path join '.dotfiles'
    }


alias dots = git $"--git-dir=($env.DOTFILES)" $"--work-tree=($env.HOME)"
dots config --local status.showUntrackedFiles no

def "dots init" [] {
    mkdir $env.DOTFILES
    git init --bare $env.DOTFILES
}

def "dots sync" [] {
    dots add -u
    dots commit -m $"dots sync: $(date now | format date '%Y-%m-%d %H:%M:%S')"
    dots push
}
