# Navigation
alias cd = z
alias ci = zi
alias zt = br
alias ct = br

# Listing Files
alias l = eza
alias ll = eza -l
alias l1 = eza -1
alias la = eza -a
alias lla = eza -la
alias l1a = eza -1a
alias lt = eza -T
alias l1t = eza -1T
alias llt = eza -lT
alias lat = eza -aT
alias llat = eza -laT
alias l1at = eza -1aT
alias tree = eza -T

# Coreutils alternatives
alias cat = bat
alias diff = delta
alias fz = fzf
alias code = codium
