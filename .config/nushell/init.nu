# Initialise starship, zoxide and broot
source ~/.config/broot/launcher/nushell/br
source ~/.cache/zoxide/init.nu
source ~/.cache/carapace/init.nu
source ~/.cache/starship/init.nu

# Import functions
source ~/.config/nushell/functions/dots.nu
source ~/.config/nushell/functions/upgrade.nu

#alias fuck = ($env.TF_ALIAS=fuck; $env.PYTHONIOENCODING=utf-8; thefuck (history | last 1) | save /home/olep/.cache/fuck.nu; nu /home/olep/.cache/fuck.nu)

# Setup fzf defaults
#$env.FZF_DEFAULT_OPTS = ""
